# Jenkins Decalarative Pipeline Syntax / Parameters
---
The **parameters** directive provides a list of parameters that a user should provide when triggering the Pipeline. The values for these user-specified parameters are made available to Pipeline steps via the params object, see the [Parameters, Declarative Pipeline](https://jenkins.io/doc/book/pipeline/syntax/#parameters-example) for its specific usage.

## Example: Parameters, Declarative Pipeline
A valid Declarative pipeline must be defined with the "pipeline" sentence 

  ```groovy
    pipeline {
      agent any
      parameters {
          string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')

          text(name: 'BIOGRAPHY', defaultValue: '', description: 'Enter some information about the person')

          booleanParam(name: 'TOGGLE', defaultValue: true, description: 'Toggle this value')

          choice(name: 'CHOICE', choices: ['One', 'Two', 'Three'], description: 'Pick something')

          password(name: 'PASSWORD', defaultValue: 'SECRET', description: 'Enter a password')
      }
      stages {
          stage('Example') {
              steps {
                  echo "Hello ${params.PERSON}"

                  echo "Biography: ${params.BIOGRAPHY}"

                  echo "Toggle: ${params.TOGGLE}"

                  echo "Choice: ${params.CHOICE}"

                  echo "Password: ${params.PASSWORD}"
              }
          }
      }
  }
  ```