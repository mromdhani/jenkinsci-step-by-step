# Jenkins Decalarative Pipeline Syntax / Tools
---
A section defining tools to auto-install and put on the PATH. This is ignored if agent none is specified.
See the [Tools Section in the official Jenkins documentation](https://jenkins.io/doc/book/pipeline/syntax/#tools) for its specific usage.

The tool name must be pre-configured in Jenkins under Manage Jenkins → Global Tool Configuration.


## Example: Tools, Declarative Pipeline

A valid Declarative pipeline must be defined with the "pipeline" sentence 

  ```groovy
    pipeline {
        agent any
        tools {
            maven 'apache-maven-3.6.3' 
        }
        stages {
            stage('Example') {
                steps {
                    bat 'mvn --version'
                }
            }
        }
}
  ```